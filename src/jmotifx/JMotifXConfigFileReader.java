/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmotifx;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author paiyeta1
 * 
 * Config file configurations
 * 
 *  databaseFilePath=./etc/databases/human.protein.aa
    peptideSequenceFile=./etc/projects/jmotifx/inputs/cptacglycosites.seqs
    backgroudSequenceFile=./etc/projects/jmotifx/inputs/dBGlycosites.seqs  
    backgroundType=input
    peptideWindow=13
    pValueCutOff=0.000001
    minimumOccurrence=20
 */
public class JMotifXConfigFileReader {
       
    public HashMap<String,String> readApplicationConfiguration(String configFilePath) throws FileNotFoundException, IOException{
        HashMap<String,String> configMap = new HashMap<String,String>();
        BufferedReader reader = new BufferedReader(new FileReader(configFilePath));
        String line;
        while((line = reader.readLine())!=null){
            String[] lineArr = line.split("=");
            String option = lineArr[0];
            String value = lineArr[1];
            configMap.put(option, value);
        }
        reader.close();
        return configMap;
    }

    void displayConfiguration(HashMap<String, String> configMap) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
}

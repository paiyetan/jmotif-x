/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmotifx.motifx;

import database.Database;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import jmotifx.JMotifXSequenceFileReader;
import jmotifx.preprocess.JMotifXBlockSequenceExtractor;

/**
 *
 * @author paiyeta1
 */
public class MotifXPeptideSequenceExtractor {

    public ArrayList<String> extractInputPeptideSeqeunces(HashMap<String, String> configMap, Database db) 
            throws FileNotFoundException, IOException {
        //throw new UnsupportedOperationException("Not yet implemented");
        ArrayList<String> inputPeptideSequences;
        String inputSequenceFile = configMap.get("peptideSequenceFile");
        JMotifXSequenceFileReader jmotifxR = new JMotifXSequenceFileReader();
        System.out.println(" MotifXPeptideSequenceExtractor: Reading sequences from file..." + new File(inputSequenceFile).getName());
        ArrayList<String> inseqs = jmotifxR.extractSequences(inputSequenceFile);
        
        //Extract block sequences....
        JMotifXBlockSequenceExtractor bseqExt = new JMotifXBlockSequenceExtractor();
        int peptideWindow = Integer.parseInt(configMap.get("peptideWindow"));
        String centerResidue = configMap.get("centerResidue");
        ArrayList<String> inseqsBlock = 
                bseqExt.extractBlockSequenceThisWindow(inseqs, peptideWindow, db, centerResidue); //window defaults to 13
        
        //Remove redundant Peptides
        MotifXPeptideSequenceFilter filter = new MotifXPeptideSequenceFilter();
        inputPeptideSequences = filter.removeRedundancy(inseqsBlock);
        
        return inputPeptideSequences;
    }
    
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmotifx.motifx;

import database.AminoAcid;
import java.util.ArrayList;
import java.util.Iterator;
import jmotifx.sequenceobjects.AroundSiteFPeptideObject;

/**
 *
 * @author paiyeta1
 */
public class MotifXPositionWeightMatrix {
    
    private char[] aASymb = {'A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y'};
    private int[] relative_positions = {-6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6};
    private int[][] matrix = new int[aASymb.length][relative_positions.length];
    
    public MotifXPositionWeightMatrix(ArrayList<AroundSiteFPeptideObject> fGPObjs){
        for (int i = 0; i < aASymb.length; i++){
            for(int j = 0; j < relative_positions.length; j++){
                matrix[i][j] = 0;
            }
        }
        estimateResidueFrequencies(fGPObjs);       
    }
    
    private void estimateResidueFrequencies(ArrayList<AroundSiteFPeptideObject> fGPObjs) {
        //throw new UnsupportedOperationException("Not yet implemented");
        Iterator<AroundSiteFPeptideObject> itr = fGPObjs.iterator();
        while(itr.hasNext()){
            AroundSiteFPeptideObject fGPObj = itr.next();
            AminoAcid[] aArr = fGPObj.getSeqArray();
            for(int i = 0; i < aArr.length; i++){
                int pos_index = getPositionIndex(aArr[i].getPosition());
                int aASymb_index = getAaSymbolIndex(aArr[i].getSymbol());
                matrix[aASymb_index][pos_index]++;
            }
        }
    }

    public int getAaSymbolIndex(char symbol) {
        //throw new UnsupportedOperationException("Not yet implemented");
        int symbolIndex = 0;
        for(int i = 0; i < aASymb.length; i++ ){
            if (symbol==aASymb[i]){
                symbolIndex = i;
            }
        }
        return symbolIndex;
    }
    
    public int getPositionIndex(int rel_position){
        int pos_index = 0;
        for(int i = 0; i < relative_positions.length; i++){
            if ( rel_position == relative_positions[i] ){
                pos_index = i;
            }
        }
        return pos_index;
    }
    
    public int[][] getMatrix(){
        return matrix;
    }
    
    public char[] getAminoAcidSymbols(){
        return aASymb;
    }
    
    public int[] getPositions(){
        return relative_positions;
    }
    
    public int getColumnSum(int col_index){
        int col_sum = 0;
        for(int i = 0; i < aASymb.length; i++){
            col_sum = col_sum + matrix[i][col_index];
        }
        return col_sum;
    }
    
    
 
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmotifx.motifx;

import database.Database;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import jmotifx.JMotifXSequenceFileReader;
import jmotifx.preprocess.JMotifXBlockSequenceExtractor;

/**
 *
 * @author paiyeta1
 */
public class MotifXBackgroundExtractor {
    
    
    public ArrayList<String> extractBGSequences(HashMap<String,String> configMap,
                                                    Database db) throws FileNotFoundException, IOException{
        ArrayList<String> blockInputBGSeqs = new ArrayList<String>();
        String inputBackgroundFile = null;
        //get type of background
        //if backgrounf type is "input", retrieve input background sequences...
        //  else,
        JMotifXBlockSequenceExtractor jmxBSXr = new  JMotifXBlockSequenceExtractor();
        String backgroundType = configMap.get("backgroundType");
        if(backgroundType.equalsIgnoreCase("input")){
            inputBackgroundFile = configMap.get("backgroudSequenceFile");
            JMotifXSequenceFileReader jmotifxR = new JMotifXSequenceFileReader();
            ArrayList<String> inputBGSeqs = jmotifxR.extractSequences(inputBackgroundFile);
            // preprocess input background sequence if not already done...
            // preprocessing at this stage involves, extracting a block sequence of the inputted sequence,
            //   presupposing the inputed peptide sequences are of variable lengths.
            blockInputBGSeqs = jmxBSXr.extractBlockSequence(inputBGSeqs, configMap, db);           
        } else if(backgroundType.equalsIgnoreCase("database")){
            //get all peptides about the specified central residue with the specified window
            blockInputBGSeqs = jmxBSXr.extractBlockSeqs4rmDBWithMotifNCentralResidue(configMap, db);          
        } else if(backgroundType.equalsIgnoreCase("random")){
            blockInputBGSeqs = jmxBSXr.extractRandomBlockSequences(configMap, db);
        }        
        return blockInputBGSeqs;
    }
    
}

package jmotifx.motifx;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import database.AminoAcid;
import java.util.ArrayList;
import java.util.Iterator;
import jmotifx.sequenceobjects.AroundSiteFPeptideObject;
import jmotifx.sequenceobjects.AroundSitePeptideObject;

/**
 *
 * @author paiyeta1
 */
public class MotifXElucidatedMotif {
    
    private ArrayList<MotifXMostSigResiduePosition> mSigRPs;
    private String motif_seq;
    private double motif_score;
    private double motif_pvalue;
    private int idSequencesSize; //number 
    private int bgSequencesSize;
    private ArrayList<AroundSiteFPeptideObject> idPepMatches;
    private ArrayList<AroundSiteFPeptideObject> bgPepMatches;
     
    public MotifXElucidatedMotif(ArrayList<MotifXMostSigResiduePosition> mSigRPs){
        this.mSigRPs = mSigRPs;
        makeMotifSequence();
        computeMotifScore();
        //id_occurrence = null;
        //bg_occurrence = null;
    }
    
    
    public MotifXElucidatedMotif(ArrayList<MotifXMostSigResiduePosition> mSigRPs, 
                  ArrayList<AroundSiteFPeptideObject> idPepObjs, 
                  ArrayList<AroundSiteFPeptideObject> bgPepObjs){
        this.mSigRPs = mSigRPs;
        this.idSequencesSize = idPepObjs.size();
        this.bgSequencesSize = bgPepObjs.size();
        makeMotifSequence();
        computeMotifScore();
        this.idPepMatches = getMatches(idPepObjs);
        this.bgPepMatches = getMatches(bgPepObjs);       
    }
    

    private void makeMotifSequence() {
        //throw new UnsupportedOperationException("Not yet implemented");
        char[] aArr = new char[13];
        for(int i = 0; i < aArr.length; i++){
            aArr[i] = 'x';
        }
        Iterator<MotifXMostSigResiduePosition> itr = mSigRPs.iterator();
        while(itr.hasNext()){
            MotifXMostSigResiduePosition mSRP = itr.next();
            char aminoAcid_symbol = mSRP.getResidueSymbol();
            int relative_position = mSRP.getResiduePosition();
            int arr_index = getArrayIndex(relative_position,aArr.length);
            aArr[arr_index] = aminoAcid_symbol;
        }        
        motif_seq = new String(aArr);
                
    }

    private int getArrayIndex(int relative_position, int length) {
        //throw new UnsupportedOperationException("Not yet implemented");
        int arr_index;
        /*
         * 
         *
        Median median = new Median();
        double[] num = new double[length];
        int num_gen = 0;
        for(int i = 0; i < length; i++ ){
            num[i] = (double) num_gen;
            num_gen++;
        }
        int mid_value = (int) median.evaluate(num,0,length);
        arr_index = mid_value + relative_position;       
        return arr_index; 
        * 
        */
        int mid_point = ((length/2) + 1);
        int abs_index = mid_point + relative_position;
        arr_index = abs_index - 1;
        return arr_index;        
    }
    
    public boolean matches(String str){
        boolean matched = true;
        char[] mSeqArr = motif_seq.toCharArray();
        char[] strArr = str.toCharArray();
        if(mSeqArr.length==strArr.length){ //expects the length of these comparing strings to match
            for(int i = 0; i < mSeqArr.length; i++){
                if(mSeqArr[i]=='x'){
                    continue;
                }else{
                    if(mSeqArr[i]!=strArr[i]){
                        matched = false;
                        break;
                    }
                }
            }
            
        }              
        return matched;
    }

    private void computeMotifScore() {
        //throw new UnsupportedOperationException("Not yet implemented");
        motif_score = 0;
        Iterator<MotifXMostSigResiduePosition> itr = mSigRPs.iterator();
        while(itr.hasNext()){
            MotifXMostSigResiduePosition mSRP = itr.next();
            motif_score = motif_score + (-(Math.log(mSRP.getProbability())));
        }
    }
    
    //get GlycPepObj that match motif 
    public ArrayList<AroundSiteFPeptideObject> getMatches(ArrayList<AroundSiteFPeptideObject> fGlycObjs){
        ArrayList<AroundSiteFPeptideObject> matches = new ArrayList<AroundSiteFPeptideObject>();
        Iterator<AroundSiteFPeptideObject> itr = fGlycObjs.iterator();
        while(itr.hasNext()){
            AroundSiteFPeptideObject fGlycObj = itr.next();
            if(this.matches(fGlycObj.getSequence())){
                matches.add(fGlycObj);
            }
        }        
        return matches;
    }
    
    public ArrayList<AroundSitePeptideObject> getMatchesHelper(ArrayList<AroundSitePeptideObject> aroundSitePeptideObjs){
        ArrayList<AroundSitePeptideObject> matches = new ArrayList<AroundSitePeptideObject>();
        Iterator<AroundSitePeptideObject> itr = aroundSitePeptideObjs.iterator();
        while(itr.hasNext()){
            AroundSitePeptideObject aroundSitePeptideObj = itr.next();
            if(this.getMatchesHelperHelper(aroundSitePeptideObj)){
                matches.add(aroundSitePeptideObj);
            }
        }       
        return matches;
    }
    
    private boolean getMatchesHelperHelper(AroundSitePeptideObject aroundSitePeptideObject) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean matches = true;
        AminoAcid[] seqArr = aroundSitePeptideObject.getAminoAcidSeqArray();
        int centerIndex = aroundSitePeptideObject.getCenterIndex();
        Iterator<MotifXMostSigResiduePosition> itr = mSigRPs.iterator();
        while(itr.hasNext()){
            MotifXMostSigResiduePosition msRP = itr.next();
            int msrp_rel_pos = msRP.getResiduePosition();
            char msrp_res_symb = msRP.getResidueSymbol();
            int index = centerIndex + msrp_rel_pos;
            if(seqArr[index].getSymbol()!= msrp_res_symb){
                matches = false;
            }
        }     
        return matches;
    }
    
    public String getMotifSequence(){
        return motif_seq;
    }
    
    public double getMotifScore(){
        return motif_score;
    }

    

    public ArrayList<AroundSiteFPeptideObject> getBgPepMatches() {
        return bgPepMatches;
    }

    public int getBgSequencesSize() {
        return bgSequencesSize;
    }

    public ArrayList<AroundSiteFPeptideObject> getIdPepMatches() {
        return idPepMatches;
    }

    public int getIdSequencesSize() {
        return idSequencesSize;
    }

    public ArrayList<MotifXMostSigResiduePosition> getmSigRPs() {
        return mSigRPs;
    }

    public double getMotif_pvalue() {
        return motif_pvalue;
    }

    public double getMotif_score() {
        return motif_score;
    }

    public String getMotif_seq() {
        return motif_seq;
    }
    
    
}

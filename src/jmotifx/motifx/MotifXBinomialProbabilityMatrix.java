/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmotifx.motifx;

//import org.apache.commons.math3.*;

import org.apache.commons.math3.distribution.BinomialDistribution;


/**
 *
 * @author paiyeta1
 */
public class MotifXBinomialProbabilityMatrix{
    
    private char[] aminoA_symbols = {'A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y'};
    private int[] relative_positions = {-6 , -5 , -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6};
    private double[][] matrix = new double[aminoA_symbols.length][relative_positions.length];
    private MotifXMostSigResiduePosition mSigResPos;
    
    
    
    public MotifXBinomialProbabilityMatrix (MotifXPositionWeightMatrix idPWM, MotifXPositionWeightMatrix bGPWM){
        for (int i = 0; i < aminoA_symbols.length; i++){
            for(int j = 0; j < relative_positions.length; j++){
                matrix[i][j] = 0.00;
            }
        }        
        computeBinomialProbabilities(idPWM, bGPWM);
        setMostSignificant();
    }

    private void computeBinomialProbabilities(MotifXPositionWeightMatrix idPWM, MotifXPositionWeightMatrix bGPWM) {
        //throw new UnsupportedOperationException("Not yet implemented");
        //P(m,c_xj,p_xj) = Sigma(i=c_xj,m) [m-Combination-i * p_xj ^i (1-p_xj)^(m-i);
        //m = number of sequences in the dataset matrix (??number of sequences used to generate idPWM)
        //c_xj = count of residue 'x' at position 'j' in the dataset matrix 
        //p_xj = fractional percentage of residue 'x' at position 'j' in the current background matrix.
        for (int i = 0; i < aminoA_symbols.length; i ++){
            for(int j = 0; j < relative_positions.length; j++){
                int m = idPWM.getColumnSum(j);
                int c_xj = idPWM.getMatrix()[i][j]; // count of residue at position j
                double p_xj = ((double) bGPWM.getMatrix()[i][j])/((double) bGPWM.getColumnSum(j));
                double bin_prob = 0.00;
                for(int trial = c_xj; trial <= m; trial++){
                    BinomialDistribution bnDistr = new BinomialDistribution(trial,p_xj);
                    bin_prob = bin_prob + bnDistr.probability(trial);
                    //int printbin = (m - c_xj)/20;
                    if(trial%10==0){
                        System.out.println("\t\t\tBinomial probability of "+ aminoA_symbols[i] + ", " + 
                                relative_positions[j] + " at iteration " + trial + " of " + m + " is " + 
                                   bin_prob);
                    }
                }
                matrix[i][j] = bin_prob;
                System.out.println("\t\t\t" + aminoA_symbols[i] + ", " +  relative_positions[j] + 
                                   " Summative Binomial probability is " + bin_prob);
            }
        }
        
    }

    private void setMostSignificant() {
        //throw new UnsupportedOperationException("Not yet implemented");
        double most_sig_prob = 1.00;
        
        //change(d) while loop to a for loop;
        for(int i = 0; i < aminoA_symbols.length; i++){
            for(int j = 0; j < relative_positions.length; j++){
                if(matrix[i][j] < most_sig_prob ){
                    most_sig_prob = matrix[i][j];
                    mSigResPos = new MotifXMostSigResiduePosition(aminoA_symbols[i],relative_positions[j],matrix[i][j]);
                }               
            }
        }
    }
    
    public MotifXMostSigResiduePosition getMostSignificant(){
        return mSigResPos;
    }
    
    public double[][] getMatrix(){
        return matrix;
    }
    
    public char[] getAminoAcidSymbols(){
        return aminoA_symbols;
    }
    
    public int[] getPositions(){
        return relative_positions;
    }
    

}

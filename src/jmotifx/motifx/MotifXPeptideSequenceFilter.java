/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmotifx.motifx;

import java.util.ArrayList;

/**
 *
 * @author paiyeta1
 */
public class MotifXPeptideSequenceFilter {
    
    public ArrayList<String> removeRedundancy(ArrayList<String> inseqsBlock) {
        //throw new UnsupportedOperationException("Not yet implemented");
        System.out.println(" Removing redundancy in sequences list...");
        ArrayList<String> nonredundant = new ArrayList<String>();
        for(String seq : inseqsBlock){
            if(nonredundant.contains(seq)==false){
                nonredundant.add(seq);
            }
        }
        return nonredundant;
    }

    
}

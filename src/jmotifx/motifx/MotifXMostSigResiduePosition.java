/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmotifx.motifx;

import database.AminoAcid;
import java.util.ArrayList;
import java.util.Iterator;
import jmotifx.sequenceobjects.AroundSiteFPeptideObject;


/**
 *
 * @author paiyeta1
 */
public class MotifXMostSigResiduePosition {
    
    private char residue_symb;
    private int relative_position;
    private double probability;
    
    public MotifXMostSigResiduePosition(char symbol, int position, double probability){
        residue_symb = symbol;
        relative_position = position;
        this.probability = probability;
    }
    
    public MotifXMostSigResiduePosition(char symbol, int position){
        residue_symb = symbol;
        relative_position = position;
    }
    
    public MotifXMostSigResiduePosition(double probability){
        this.probability = probability;
        //residue_symb = null;       
    }
    
    /*
     * 
     *
    public void setResidueSymbol(char c){
        residue_symb = c;
    }
    
    public void setResiduePosition(int pos){
        this.pos = pos;
    }
    
    public void setProbability(double prob){
        this.prob = prob;
    }
    * 
    */
    
    public char getResidueSymbol(){
        return residue_symb;
    }
    
    public int getResiduePosition(){
        return relative_position;
    }
    
    public double getProbability(){
        return probability;
    }
    
    public int getOccurrence(ArrayList<AroundSiteFPeptideObject> fGlycObjs){
        int occurrence = 0;
        //implement
        Iterator<AroundSiteFPeptideObject> itr = fGlycObjs.iterator();
        while(itr.hasNext()){
            AroundSiteFPeptideObject fGlycObj = itr.next();
            if(inFGlycObjAaSeqArray(residue_symb,relative_position,fGlycObj)){
                occurrence++;
            }
        }
        
        return occurrence;
    }

    public boolean inFGlycObjAaSeqArray(char symb, int rel_pos, AroundSiteFPeptideObject fGlycObj) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean inAaSeqArray = false;
        AminoAcid[] aArr = fGlycObj.getSeqArray();
        for(int i = 0; i < aArr.length; i++){
            if(symb==aArr[i].getSymbol() && rel_pos==aArr[i].getPosition()){
                inAaSeqArray = true;
                break;
            }
        }
        
        
        return inAaSeqArray;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmotifx.preprocess;

/**
 *
 * @author paiyeta1
 */
public class JMotifXBlockSequence {
    
    private String block;
    private String peptide;
    private String proteinAcc;
    private int nxstLocation;

    public JMotifXBlockSequence(String block, String peptide, String proteinAcc) {
        this.block = block;
        this.peptide = peptide;
        this.proteinAcc = proteinAcc;
    }

    public JMotifXBlockSequence(String block, String peptide, String proteinAcc, int nxstLocation) {
        this.block = block;
        this.peptide = peptide;
        this.proteinAcc = proteinAcc;
        this.nxstLocation = nxstLocation;
    }

    public String getBlock() {
        return block;
    }

    public int getNxstLocation() {
        return nxstLocation;
    }

    public String getPeptide() {
        return peptide;
    }

    public String getProteinAcc() {
        return proteinAcc;
    }
    
    
    
}

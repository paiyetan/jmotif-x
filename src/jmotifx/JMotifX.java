/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmotifx;

import database.Database;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import utilities.OneFileChooser;
import utilities.OutputDirChooser;
import jmotifx.motifx.MotifXElucidatedMotif;
import jmotifx.motifx.MotifX;

/**
 *
 * @author paiyeta1
 */
public class JMotifX {
    
    public static void main(String[] args) throws FileNotFoundException, IOException{
        System.out.println("Starting...");
        
        
        /*
        //select (input) database
        OneFileChooser dbchooser = new OneFileChooser("Select database...");
        String dbPath = dbchooser.getInputFile();
        //select output directory
        OutputDirChooser outchooser = new OutputDirChooser("Select output directory...");
        String outDir = outchooser.getOutputDir();
        
        Database db = new Database(dbPath);
        
        System.out.println("Computing MotifX...");
        MotifX motifX = new MotifX(db, outDir);
        LinkedList<Motif> motifs = motifX.getMotifsElucidated();
        motifX.printMotifElucidationSummary(motifs, outDir + File.separator + "xGJMotifX.summary");
        * 
        */
        
        
        //read configuration file (anticipated to contain all information and options to run application) 
        JMotifXConfigFileReader configReader = new JMotifXConfigFileReader();
        HashMap<String,String> configMap = configReader.readApplicationConfiguration("./JMotifX.config");
        //configReader.displayConfiguration(configMap);
        
        //run main application
        MotifX motifX = new MotifX(configMap);
        LinkedList<MotifXElucidatedMotif> elucidatedMotifs  = motifX.getMotifsElucidated();
        motifX.printMotifElucidationSummary(elucidatedMotifs, configMap.get("outputDirectoryPath") + File.separator + "JMotifX.summary");
        
        
        System.out.println("...Done!");
        
    }
    
}

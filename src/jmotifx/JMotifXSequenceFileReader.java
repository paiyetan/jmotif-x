/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmotifx;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author paiyeta1
 */
public class JMotifXSequenceFileReader {
    
    public ArrayList<String> extractSequences(String sequenceFile) throws FileNotFoundException, IOException{
        ArrayList<String> sequences = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(sequenceFile));
        String sequence = null;
        while((sequence = reader.readLine())!= null){
            sequences.add(sequence);
        }
        reader.close();
        return sequences;
    }
    
    public HashMap<String,LinkedList<String>> extractPep2ProtAccnsMap(String sequenceFile) 
            throws FileNotFoundException, IOException{
        HashMap<String,LinkedList<String>> pep2ProtAccnsMap = new HashMap<String,LinkedList<String>>();
        BufferedReader reader = new BufferedReader(new FileReader(sequenceFile));
        String sequenceLine = null;
        int linesRead = 0;
        try{
            while((sequenceLine = reader.readLine())!= null){
                linesRead++;
                if((linesRead > 0) && (sequenceLine.isEmpty()==false)){ //skip headerLine
                    String[] lineArr = sequenceLine.split("\t");
                    String sequence = lineArr[0];
                    String protAccn = lineArr[1];
                    //pep2ProtAccnsMap.put(sequence, protAccn);
                    if(pep2ProtAccnsMap.containsKey(sequence)){
                        LinkedList<String> accns = pep2ProtAccnsMap.remove(sequence);
                        accns.add(protAccn);
                        pep2ProtAccnsMap.put(sequence, accns);
                    }else{
                        LinkedList<String> accns = new LinkedList<String>();
                        accns.add(protAccn);
                        pep2ProtAccnsMap.put(sequence, accns);
                    }
                }
            }
        }catch(Exception e){
           System.out.println("Line content: " + sequenceLine + "; Line number: " + linesRead);
           e.printStackTrace();
        }
        reader.close();
        return pep2ProtAccnsMap;
    }

    
    
}

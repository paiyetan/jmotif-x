/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmotifx.sequenceobjects;

import database.AminoAcid;

/**
 *
 * @author paiyeta1
 */
public class AroundSitePeptideObject {
    
    private String sequence;
    private int length;
    private int centerIndex;
    private int preCenterLength;
    private int postCenterLength;   
    private AminoAcid[] seqArray; 
    private MissedCleavage mCleave; 
    
    
    public AroundSitePeptideObject(String seq, String center){
        sequence = seq;
        length = seq.length();
        setCenterIndex(seq, center);
        setPreCenterLength();
        setPostCenterLength(seq); 
        setAminoAcidArray(seq);
        setMissedCleavage(seq);
    }

    private void setCenterIndex(String seq, String center) {        
         centerIndex = seq.indexOf(center);      
    }

    private void setPreCenterLength() {
        //throw new UnsupportedOperationException("Not yet implemented");
        preCenterLength = 0;
        while(preCenterLength != centerIndex){
            preCenterLength++;
        }
    }

    private void setPostCenterLength(String seq) {
        //throw new UnsupportedOperationException("Not yet implemented");
        postCenterLength = seq.length() - (centerIndex + 1);
    }
    
    private void setAminoAcidArray(String seq){
        int index = 0;
        seqArray = new AminoAcid[seq.length()];
        while (index < seq.length()){
            char symb = seq.charAt(index);
            boolean isCenter = false;
            if(index == centerIndex){ 
                isCenter = true;
            }
            int rel_pos = index - centerIndex;
            seqArray[index]  = new AminoAcid(symb, isCenter, rel_pos);
            index++;           
        }        
    }
    
    private void setMissedCleavage(String seq){
        mCleave = new MissedCleavage(seq);    
    }
    
    public String getSequence(){
        return sequence;
    }
    
    public int getPeptideLength(){
        return length;
    }
    
    public int getPreCenterLength(){
        return preCenterLength;
    }
    
    public int getPostCenterLength(){
        return postCenterLength;
    }
    
    public AminoAcid[] getAminoAcidSeqArray(){
        return seqArray; 
    }
        
    public MissedCleavage getMissedCleavage(){
        return mCleave;       
    }
    
    public int getNoOfMissedCleavages(){
        return mCleave.noOfMissedCleavages;
    }
    
    public boolean hasMissedCleavages(){
        boolean has = false;
        if (mCleave.noOfMissedCleavages > 0){
            has = true;
        }
        return has;
    }
    
    public int getCenterIndex(){
        return centerIndex;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmotifx.sequenceobjects;

import database.AminoAcid;

/**
 *
 * @author paiyeta1
 */
public class AroundSiteFPeptideObject
{ 
    
    private String sequence;
    private AminoAcid[] seqArray;
    private AroundSitePeptideObject parent;
    
    public AroundSiteFPeptideObject(AminoAcid[] aa, AroundSitePeptideObject par){
        seqArray = aa;
        parent = par;
        setSequence();
    }
    
    public AroundSiteFPeptideObject(AminoAcid[] aa){
        seqArray = aa;
        setSequence();
    }
    
    public AroundSiteFPeptideObject(AroundSitePeptideObject aspo){
        seqArray = aspo.getAminoAcidSeqArray();
        sequence = aspo.getSequence();
    }
    
    public AroundSiteFPeptideObject(String seq, String centerResidue){
        this(new AroundSitePeptideObject(seq, centerResidue));        
    }

    private void setSequence() {
        //throw new UnsupportedOperationException("Not yet implemented");
        char[] cArr = new char[seqArray.length];
        for(int i = 0; i < seqArray.length; i++){
            AminoAcid aAcid = seqArray[i];
            cArr[i] = aAcid.getSymbol();
        }
        sequence = new String(cArr);
    }
    
    public AminoAcid[] getSeqArray(){
        return seqArray;
    }
    
    public AroundSitePeptideObject getParent(){
        return parent;
    }
    
    public String getSequence(){
        return sequence;
    }

}

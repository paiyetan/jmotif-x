/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.util.ArrayList;

/**
 *
 * @author paiyeta1
 */
public class Protein {
    
    private String titleLine;
    private String displayId;
    private String accession;
    private String description;
    private String sequence;
    private ArrayList<Integer> glycosite_indeces; //predicted glycosite_indeces
    
    //field stubs
    private String proteinName;
    //private String genebank_acc;
    //private String unipro_acc;
    //private String organism;

    
    public Protein(String titleLine, String sequence, String dbName){
        this.titleLine = titleLine;
        this.sequence = sequence;
        //setDetails(titleLine,dbName); //dbName helps in the database-specific parsing 
        setGlycositeIndeces();
    }
    
    
    public void setDetails(String titleLine, String dbName) {
        //throw new UnsupportedOperationException("Not yet implemented");
        int switch_int = 0;
        if (dbName.equalsIgnoreCase("ipi.HUMAN.fasta.v2.28")){ switch_int = 1;}
        switch(switch_int){ //strings in switch are not supported for java 1.6 but in 1.7 or higher
            case 1: //not yet implemented - place holder for other dbases
                    //displayID
                    //accession
                    //description
            //case 2: //not yet implemented - place holder for other dbases     
            default:
        }
        
    }

    private void setGlycositeIndeces() {
        //throw new UnsupportedOperationException("Not yet implemented");
        glycosite_indeces = new ArrayList<Integer>();
        for(int i = 0; i < sequence.length()-2; i++ ){
            if(sequence.charAt(i)=='N' && ((sequence.charAt(i+2)=='S')||(sequence.charAt(i+2)=='T'))){
                glycosite_indeces.add(i);
            }
        }
    }
    
    public String getTitleLine() {
        return titleLine;
    }

    public String getDisplayId() {
        return displayId;
    }
    
    public String getAccession() {
        return accession;
    }

    public String getDescription() {
        return description;
    }

    public String getSequence() {
        return sequence;
    }

    public ArrayList<Integer> getGlycositeIndeces() {
        return glycosite_indeces;
    }

    public String getProteinName() {
        return proteinName;
    }
    
    public String getReversedSequence(){
        char[] reverseStringArray = new char[sequence.length()];
        for(int i = sequence.length() - 1, j = 0; i != -1; i--, j++ ){
            reverseStringArray[j] = sequence.charAt(i);
        }
        return new String(reverseStringArray);
    }
    
    public ArrayList<Integer> getResidueIndeces(String residue) {      
        ArrayList<Integer> indeces = new  ArrayList<Integer>();
        for(int i = 0; i < sequence.length(); i++ ){
            if(sequence.charAt(i)== residue.charAt(0)){
                indeces.add(i);
            }
        }
        return indeces;
    } 

 
    
        
    
    
}

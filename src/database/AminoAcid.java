/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author paiyeta1
 */
public class AminoAcid {
    
    private char symbol;
    private int count;
    private boolean isCenter;
    private int relative_position; // relative position to Glycosite
    
    public AminoAcid(char symbol){
        this.symbol = symbol;
        count = 0;
    }
    
    public AminoAcid(char symbol, boolean isGlyc, int rel_pos){
        this.symbol = symbol;
        this.isCenter = isGlyc;
        relative_position = rel_pos;       
    }

    public char getSymbol() {
        return symbol;
    }

    public int getCount() {
        return count;
    } 
    
    public boolean isCenter(){
        return isCenter;
    }
        
    public void increaseCount(){
        count++;
    } 
    
    public int getPosition(){
        return relative_position; 
    }
    
}

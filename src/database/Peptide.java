/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author paiyeta1
 */
public class Peptide {
    
    private String type; //tryptic or else
    private String sequence;
    private boolean glycosite;
    
    public Peptide(String type, String sequence, boolean containsNXST){
        this.type = type;
        this.sequence = sequence;
        this.glycosite = containsNXST;
    }

    public String getType(){
        return type;
    }
    
    public String getSequence(){
        return sequence;
    }

    public boolean isGlycosite(){
        return glycosite;
    }
      
}

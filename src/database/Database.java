/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
//import xMotifR.GlycPepObj;

/**
 *
 * @author paiyeta1
 */
public class Database {
    
    private String name; 
    private ArrayList<Protein> proteins;
    private AminoAcid[] aminoAcids;
    
    public Database(String fullPath, String dbType) throws FileNotFoundException, IOException{
        //name = new File(fullPath).getName();
        name = dbType;
        proteins = new ArrayList<Protein>();
        setAminoAcids();
        readDB(fullPath);
        setDBAminoAcidCounts();       
    }
    
    public Database(String fullPath) throws FileNotFoundException, IOException{
        proteins = new ArrayList<Protein>();
        setAminoAcids();
        readDB(fullPath);
        setDBAminoAcidCounts();       
    }
    
    private void setAminoAcids() {
        aminoAcids = new AminoAcid[20];
        char[] symb = {'A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y'};
        for (int i = 0; i < symb.length; i++){
            aminoAcids[i] = new AminoAcid(symb[i]);
        }
    }
    
    private void readDB(String dbPath) throws FileNotFoundException, IOException {
        System.out.println("  Reading-in local file Database...");
        BufferedReader in = new BufferedReader(new FileReader(new File(dbPath)));
        String line;
        //read everyLine into an arrayList of Strings
        //parse ArrayList to Arrays
        //by accessing arrays read line and components       
        ArrayList<String> dbLines = new ArrayList<String>();
        while((line = in.readLine())!=null){
            dbLines.add(line);                       
        }
        readDBHelper(dbLines);
              
    }
    
    private void readDBHelper(ArrayList<String> dbLines){
        String[] lines = new String[dbLines.size()];
        Iterator<String> itr = dbLines.iterator();
        int line = 0;
        while(itr.hasNext()){
            String dbLine = itr.next();
            lines[line] = dbLine;
            line++;
        }
        readDBHelper2(lines);
    }
    
    
    private void readDBHelper2(String[] dbLines){
        for(int i = 0; i < dbLines.length; i++){
            if (dbLines[i].charAt(0)=='>') { //that's a header
                String titleLine = dbLines[i];
                int j = i+1;
                String protein_seq = dbLines[j];
                while(( (j+1) != dbLines.length) && (dbLines[j+1].charAt(0)!='>')){
                    protein_seq = protein_seq + dbLines[j+1];
                    j++;
                }
                proteins.add(new Protein(titleLine, protein_seq, name));
                if(proteins.size()%5000==0){
                    System.out.println("   "+proteins.size()+" protein entries read");
                }
                
            }
        }
    }

    private void setDBAminoAcidCounts() {
        //iterate through the protein sequences and count the amino acids
        Iterator<Protein> itr = proteins.iterator();
        while(itr.hasNext()){
            Protein protein = itr.next();
            String protein_seq = protein.getSequence();
            char[] c = protein_seq.toCharArray();
            for(int i = 0; i < c.length; i++){
                setDBAminoAcidCharCount(c[i]);
            }
        }
    }

    private void setDBAminoAcidCharCount(char c) {
        for(int i = 0; i < aminoAcids.length; i++){
            if (c == aminoAcids[i].getSymbol()){
                aminoAcids[i].increaseCount();
            }
        }
    }
    
   
    public ArrayList<Protein> getProteins(){
        return proteins;
    }
    
    public AminoAcid[] getAminoAcids(){
        return aminoAcids;
    }
    
   
    
    public void printDBPropertiesXML(String outputFolderPath) throws ParserConfigurationException, TransformerConfigurationException, TransformerException{
        //throw new UnsupportedOperationException("Not yet implemented");
        //file ouputs an xml file containing the database properties.
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        
        //make Element
        Element db_root = doc.createElement("db_properties");
        Element db_name = doc.createElement("db_name");
        Element db_size = doc.createElement("db_size");
        Element db_aacids = doc.createElement("db_aacids");
        
        doc.appendChild(db_root);
        db_root.appendChild(db_name);
        db_name.appendChild(doc.createTextNode(name));
        db_root.appendChild(db_size);
        db_size.appendChild(doc.createTextNode(String.valueOf(proteins.size())));
        db_root.appendChild(db_aacids);
               
        //for each of the amino acid in DB make a tag_element
        for(int i = 0; i < aminoAcids.length; i++){
            Element db_aacid = doc.createElement("db_aacid");
            Attr attr = doc.createAttribute("symbol");
            attr.setValue(String.valueOf(aminoAcids[i].getSymbol()));
            Attr attr2 = doc.createAttribute("frequency");
            attr2.setValue(String.valueOf(aminoAcids[i].getCount()));
            db_aacid.setAttributeNode(attr);
            db_aacid.setAttributeNode(attr2);
            db_aacid.appendChild(doc.createTextNode(String.valueOf(aminoAcids[i].getSymbol())));
            db_aacids.appendChild(db_aacid);
        }
        
        //write content out 
        TransformerFactory tfactory  = TransformerFactory.newInstance();
        Transformer transformer = tfactory.newTransformer();
        StreamResult result = new StreamResult(new File(outputFolderPath + File.separator + name +".dbproperties.xml"));
        
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        
        System.out.println("\t\tDatabase properties outputted to " + 
                outputFolderPath + File.separator + name + ".dbproperties.xml");
                
    }
    
    /*
    public ArrayList<GlycPepObj> getDBBackgroundGlycPep() {
        //throw new UnsupportedOperationException("Not yet implemented");
        ArrayList<GlycPepObj> bGGlycPep = new ArrayList<GlycPepObj>();
        //ArrayList<Protein> proteins = dbase.getProteins(); 
        Iterator<Protein> itr = proteins.iterator();
        System.out.println("\t\tGetting DB proteins");
        int proteins_searched = 0;
        while(itr.hasNext()){
            Protein  protein = itr.next();
            proteins_searched++;
            ArrayList<Integer> glycosites = protein.getGlycositeIndeces();
            Iterator<Integer> itr2 = glycosites.iterator();
            while(itr2.hasNext()){
               int glyc_loc = itr2.next(); 
               //get pretryptic chars;
               int pre_nxst_tryptic_site = glyc_loc;
               int post_nxst_tryptic_site = glyc_loc;
               while((pre_nxst_tryptic_site != 0)){
                   if(protein.getSequence().charAt(pre_nxst_tryptic_site-1) != 'R'){
                        pre_nxst_tryptic_site--; 
                   }
                   if(protein.getSequence().charAt(pre_nxst_tryptic_site-1) == 'R'){
                       break;
                   }
               }
               while((post_nxst_tryptic_site < protein.getSequence().length()) && 
                       (protein.getSequence().charAt(post_nxst_tryptic_site) != 'R')){
                   post_nxst_tryptic_site++; 
                   if(protein.getSequence().charAt(post_nxst_tryptic_site) == 'R'){
                       break;
                   }
               }
               String glyc_seq = protein.getSequence().substring(pre_nxst_tryptic_site, post_nxst_tryptic_site);
               bGGlycPep.add(new GlycPepObj(glyc_seq));              
            }
        }
        if (proteins_searched%1000==0){
            System.out.println("\t\t  " + proteins_searched + " proteins searched");
        }
        return bGGlycPep;
    }
    * 
    */

    public HashMap<String, String> getProtTitleLine2ProtSequenceMap() {
        System.out.println(" Mapping database \"proteins' title line\" to proteins' respective sequence....");
        HashMap<String,String> protTitleLine2ProtSequenceMap = new HashMap<String,String>();
        for(Protein prot : proteins){
            String tLine = prot.getTitleLine();
            String sequence = prot.getSequence();
            protTitleLine2ProtSequenceMap.put(tLine, sequence);
        }
        return protTitleLine2ProtSequenceMap;
    }
    
   
    
    
}

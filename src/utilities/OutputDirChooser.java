/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

/**
 *
 * @author paiyeta1
 */
public class OutputDirChooser extends JPanel {
    
    private String outDir;
	
	
	public OutputDirChooser(String dialogTitle){
		setOutputDir(dialogTitle);
		
	}
        
        //public OutputDirChooser(Stri)

	private void setOutputDir(String dialogTitle) {
		// TODO Auto-generated method stub
		JFileChooser fc = new JFileChooser();
		
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		//fc.setFileHidingEnabled(false);
		fc.setDialogTitle(dialogTitle);
		fc.setMultiSelectionEnabled(false);
		//fc.showOpenDialog(InputFileChooser.this);
		fc.showDialog(OutputDirChooser.this, "Select");
		File file = fc.getSelectedFile();
		outDir = file.getAbsolutePath();		
		
		//JDialog dialog = fc.createDialog(new JTextField());
		
	}
	
        public String getOutputDir(){
		return outDir;
	}
	
    
}

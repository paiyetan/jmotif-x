/**
 * 
 */
package utilities;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

/**
 * @author paiyeta1
 *
 */
@SuppressWarnings("serial")
public class MultiFileChooser extends JPanel {
	
	//private JPanel fileChooser = new JPanel();
	private String[] inputFiles;
	
	
	public MultiFileChooser(String dialogTitle){
		setInputFiles(dialogTitle);
		
	}

	private void setInputFiles(String dialogTitle) {
		// TODO Auto-generated method stub
		JFileChooser fc = new JFileChooser();
		
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		//fc.setFileHidingEnabled(false);
		fc.setDialogTitle(dialogTitle);
		fc.setMultiSelectionEnabled(true);
		//fc.showOpenDialog(InputFileChooser.this);
		fc.showDialog(MultiFileChooser.this, "Select");
		File[] files = fc.getSelectedFiles();
		inputFiles = new String[files.length];
		for(int i = 0; i < inputFiles.length; i++){
			inputFiles[i] = files[i].getAbsolutePath();
		}
		
		//JDialog dialog = fc.createDialog(new JTextField());
		
	}
	
        public String[] getInputFiles(){
		return inputFiles;
	}
	
	

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

/**
 *
 * @author paiyeta1
 */
public class OneFileChooser extends JPanel{

	//private JPanel fileChooser = new JPanel();
	private String inputFile;
	
	
	public OneFileChooser(String dialogTitle){
		setInputFiles(dialogTitle);
		
	}

	private void setInputFiles(String dialogTitle) {
		// TODO Auto-generated method stub
		JFileChooser fc = new JFileChooser();
		
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		//fc.setFileHidingEnabled(false);
		fc.setDialogTitle(dialogTitle);
		fc.setMultiSelectionEnabled(false);
		//fc.showOpenDialog(InputFileChooser.this);
		fc.showDialog(OneFileChooser.this, "Select");
		File file = fc.getSelectedFile();
		inputFile = file.getAbsolutePath();
		
	}
	
        public String getInputFile(){
		return inputFile;
	}
	
	    
    
}
